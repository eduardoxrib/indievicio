### Indie Vicio

Projeto criado para estudo inicial de React e Firebase para a cadeira de Programação para Dispositivos Móveis.
IndieVicio é um site de notícias focado em jogos e projetos indie.

## Features

- Listagem de Noticias na tela inicial via Cards (Bootstrap)
- Mostra data, hora e autor da noticia
- Simulação de login para comentários
- Seção de comentários em cada notícia
- Data e hora formatados pela biblioteca Moment.js

## Inicialização

- Clone o repositório para o diretório que desejar
- Rode o comando: npm install (ou instale a node modules com o gerenciador de pacotes de sua preferência)
- Após a instalação do node_modules, inicie o servidor local com o comando: npm start
- Acesse o endereço: http://localhost:3000 no browser de sua preferência