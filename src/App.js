import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from './telas/Header'
import Noticias from './telas/Noticias'
import Noticia from './telas/Noticia'
import Footer from './telas/Footer'

function App() {
  return (
    //<div style={{ backgroundImage: "url('https://media.istockphoto.com/vectors/gadget-icons-vector-seamless-pattern-hand-drawn-doodle-computer-game-vector-id983312018')" }}>
    <div style={{ backgroundImage: "url('https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX12099251.jpg')" }}>
      <Router>
        <Header />
        <Route path="/" exact component={Noticias} />
        <Route path="/noticia/:id" component={Noticia} />
        <Footer />
      </Router>
    </div>
  );
}

export default App;
