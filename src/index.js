import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import * as firebase from 'firebase/app'
import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyBA07AVfq8J9V164jzbIY9MriAwJyB57qA",
    authDomain: "trabalho3-b9403.firebaseapp.com",
    databaseURL: "https://trabalho3-b9403.firebaseio.com",
    projectId: "trabalho3-b9403",
    storageBucket: "trabalho3-b9403.appspot.com",
    messagingSenderId: "243530651949",
    appId: "1:243530651949:web:338f47ac1073d1d80c9ba4",
    measurementId: "G-0Z1PE8G2VG"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));