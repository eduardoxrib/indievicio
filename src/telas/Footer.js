import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Header extends Component {
    render() {
        return (
            <nav className="container sm-12" style={{ backgroundColor: 'black', color: 'white', padding: '10px' }}>
                <img src="http://pngimg.com/uploads/facebook_logos/facebook_logos_PNG19751.png" alt="" style={{ width: '100px' }} />
                <img src="http://pluspng.com/img-png/youtube-transparent-png-image-512.png" alt="" style={{ width: '85px' }} />
                <img src="https://icon-library.net/images/icon-twitter/icon-twitter-9.jpg" alt="" style={{ width: '70px', marginLeft: '10px' }} />
                <h6>© 2019 IndieVicio - O maior site de Jogos do Brasil!</h6>
            </nav>
        )
    }
}
