import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-sm bg-success navbar-dark" style={{ boxShadow: '3px black' }}>
                <Link className="navbar-brand" to="/">
                    <img src="" alt="" /> IndieVicio
                </Link>
            </nav>
        )
    }
}
