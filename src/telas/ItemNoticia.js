import React from 'react'
import { Link } from 'react-router-dom'

// key = { noticia.id }
// id = { noticia.id }
// data = { noticia.data }
// icone = { noticia.icone }
// autor = { noticia.autor }
// titulo = { noticia.titulo }

const ItemNoticia = props => (
    <div className="card" style={{ width: '300px', height: '400px', textAlign: 'center' }}>
        <Link to={`/noticia/${props.id}`}>
            <img className="card-img-top" src={props.icone} alt='Tela' style={{ width: '220px' }} />
        </Link>
        <div className="card-body">
            <h4 className="card-title">{props.titulo}</h4>
            <p className="card-text" style={{ float: 'bottom' }}>Autor: {props.autor}<br /></p>
        </div>
    </div>
)

export default ItemNoticia