import React, { Component } from 'react'
import * as firebase from 'firebase/app'
import 'firebase/firestore'

import ItemNoticia from './ItemNoticia'

class Noticias extends Component {
    constructor(props) {
        super(props)

        this.state = {
            noticias: [],
        }
    }

    componentDidMount = () => {
        this.loadNoticias()
    }

    loadNoticias = () => {
        let noticias = []

        firebase.firestore().collection('Noticia').orderBy('titulo').onSnapshot(snapshot => {

            snapshot.docChanges().forEach(change => {

                if (change.type === "added") {
                    noticias.push({ id: change.doc.id, ...change.doc.data() })
                }

                if (change.type === "modified") {
                    noticias = noticias.map(noticia => noticia.id === change.doc.id ?
                        { id: change.doc.id, ...change.doc.data() }
                        : noticia)
                }

                if (change.type === "removed") {
                    noticias = noticias.filter(noticia => noticia.id !== change.doc.id)
                }
            })
            this.setState({ noticias })
        })
    }

    render() {
        return (
            <div className="container sm-12" style={{ backgroundColor: 'white' }}>
                <br />
                <div className="container sm-4" style={{ marginTop: '2vh' }}>
                    <div className='card-columns'>
                        {this.state.noticias.map((noticia) => (
                            <ItemNoticia key={noticia.id}
                                id={noticia.id}
                                data={noticia.data}
                                icone={noticia.icone}
                                autor={noticia.autor}
                                titulo={noticia.titulo} />
                        ))}
                    </div>
                </div>
            </div >
        )
    }
}

export default Noticias
