import React from 'react'
import { Link } from 'react-router-dom'
import * as Moment from 'moment'

const Comentario = props => (
    <div className="container" style={{ height: '160px', textAlign: 'center', verticalAlign: 'center', borderWidth: '1px', borderRadius: '4px', borderColor: 'green', marginTop: '2vw', marginBottom: '1vw', borderStyle: 'solid solid outset', backgroundColor: 'white' }}>
        <div className="row" style={{ padding: '6px' }}>
            <div className="col-sm-2">
                <img src={props.img} alt="" style={{ width: '120px', height: '120px', borderWidth: '2px', borderColor: 'green', padding: '2px', borderStyle: 'solid solid outset' }} />
            </div>
            <div className="col-sm-10">
                <h6><b>{props.autor}:</b></h6> <br />
                <h6>{props.texto}</h6>
                <p style={{ textAlign: 'right', fontSize: '0.8vw' }}>{Moment(props.datah).format("LLL")}</p>
            </div>
        </div>
    </div>
)

export default Comentario