import React, { Component } from 'react'
import * as firebase from 'firebase/app'
import * as Moment from 'moment'

import { Link } from 'react-router-dom'

import Comentario from './Comentario'
import { timingSafeEqual } from 'crypto'

class Noticia extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: null,
            data: null,
            icone: null,
            autor: null,
            titulo: null,
            imgHeader: null,
            texto: null,
            comentarios: [],

            user: '',
            userID: null,
            foto: null,
            comentario: '',

            aviso: '',
        }
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    componentDidMount = () => {
        const { match: { params } } = this.props

        const db = firebase.firestore()

        var docRef = db.collection("Noticia").doc(params.id);

        docRef.get().then(doc => {
            if (doc.exists) {
                let data = Moment(Date(doc.data).toString()).format("LLL")
                this.setState({ id: doc.id, ...doc.data(), data })
            } else {
                // doc.data() will be undefined in this case
                console.log("Erro...");
            }
        }).catch(function (error) {
            console.log("Erro de conexão: ", error);
        });

        this.loadComentarios(params.id);

    }

    handleSubmit = e => {
        e.preventDefault()

        if (this.state.comentario.length < 3) {
            this.Aviso("O comentário deve ter no mínimo 3 caracteres!")
            return
        }

        if (this.state.user === "") {
            this.Aviso("Você deve estar logado para fazer um comentário")
            return
        }

        const comentario = {
            autor: this.state.user,
            img: this.state.foto,
            texto: this.state.comentario,
            data: Date(),
        }

        const db = firebase.firestore()

        try {
            db.collection('Noticia').doc(this.state.id)
                .collection('Comentarios').add(comentario)
            this.Aviso('Comentário Adicionado =D')
        } catch (erro) {
            this.Aviso('Erro: ' + erro)
        }
        this.setState({ comentario: '' })
    }

    Aviso = (txt) => {
        this.setState({ aviso: txt })
        setTimeout(() => {
            this.setState({ aviso: '' })
        }, 5000)
    }

    loadComentarios = (paramID) => {
        let comentarios = []

        firebase.firestore().collection('Noticia').doc(paramID).collection('Comentarios').orderBy('data').onSnapshot(snapshot => {

            snapshot.docChanges().forEach(change => {

                if (change.type === "added") {
                    comentarios.push({ id: change.doc.id, ...change.doc.data() })
                }

                if (change.type === "modified") {
                    comentarios = comentarios.map(comentario => comentario.id === change.doc.id ?
                        { id: change.doc.id, ...change.doc.data() }
                        : comentario)
                }

                if (change.type === "removed") {
                    comentarios = comentarios.filter(comentario => comentario.id !== change.doc.id)
                }
            })
            this.setState({ comentarios })
        })
    }

    login = () => {
        let user = prompt('Digite o nome de usuário: (João, Nicolas, Jonas Fox, Linus, Alex)', '')

        firebase.firestore().collection("Users").where("user", "==", user)
            .get()
            .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                    // doc.data() is never undefined for query doc snapshots
                    console.log(doc.id, " => ", doc.data());
                    this.setState({ userID: doc.id, ...doc.data() });
                });
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
            });
    }

    render() {
        return (
            <div className="container" style={{ backgroundColor: '#F6FFFC' }}>

                <div hidden={this.state.user === "" ? false : true}>
                    <input type="button" value="Login" class="btn btn-success mt-4" onClick={this.login} />
                </div>
                <div hidden={this.state.user === "" ? true : false} class="mt-4">
                    <img src={this.state.foto} alt="perfil" style={{ width: '100px', heigth: '120px', display: 'block' }} />
                    <h4>Olá {this.state.user}</h4>
                </div>

                <div className="col-sm-12" style={{ textAlign: 'center', marginTop: '1vw' }}>
                    <h1>{this.state.titulo}</h1>
                </div>
                <div className="col-sm-6">
                    <h6>Escrito por: {this.state.autor}</h6>
                    <h6>Postado: {this.state.data}</h6>
                </div>
                <div className="col-sm-12">
                    <img src={this.state.imgHeader} alt="Jogo" style={{ width: '80%', height: '60vh', display: 'block', marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
                <div className="col-sm-12" style={{ marginTop: "1vw" }}>
                    <p>{this.state.texto}</p>
                </div>

                <div className="container sm-12 mt-2" style={{ width: '100%', borderWidth: '1px', borderColor: 'green', borderRadius: '4px', padding: '20px', borderStyle: 'solid solid outset', backgroundColor: 'white' }}>
                    <form onSubmit={this.handleSubmit}>
                        <div className="container sm-12">
                            <div className="row">
                                <h4>Comentar</h4>
                            </div>
                            <div className="row">
                                <div className="col-sm-10">
                                    <div className="input-group mt-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="fas fa-comments"></i>
                                            </span>
                                        </div>
                                        <input type="text" className="form-control"
                                            placeholder="Comentário"
                                            name="comentario"
                                            onChange={this.handleChange}
                                            value={this.state.comentario}
                                        />
                                    </div>
                                </div>
                                <div className="col-sm-2">
                                    <input type="submit" className="btn btn-danger float-right mt-3"
                                        value="Comentar" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                {this.state.aviso !== '' ?
                    <div className='alert alert-info mt-3'>
                        {this.state.aviso}
                    </div>
                    : ''
                }

                <div className="container sm-12 mt-2" >
                    <h4>Comentários:</h4>
                    {this.state.comentarios.length === 0 ?
                        <div style={{ marginTop: '2vw' }}>
                            <h6>Ainda ninguém comentou, seja o primeiro!</h6>
                        </div>
                        : ''}
                    <div className=''>
                        {this.state.comentarios.map((comentario) => (
                            <Comentario key={comentario.id}
                                id={comentario.id}
                                img={comentario.img}
                                texto={comentario.texto}
                                autor={comentario.autor}
                                datah={comentario.data}
                            />
                        ))}
                    </div>
                    <div style={{ height: '20px' }}>

                    </div>
                </div>
            </div>
        )
    }
}

export default Noticia
